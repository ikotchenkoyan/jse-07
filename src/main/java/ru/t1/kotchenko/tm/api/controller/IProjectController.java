package ru.t1.kotchenko.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
