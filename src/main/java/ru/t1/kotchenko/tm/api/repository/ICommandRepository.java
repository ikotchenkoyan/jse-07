package ru.t1.kotchenko.tm.api.repository;

import ru.t1.kotchenko.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommand();

}
