package ru.t1.kotchenko.tm.service;

import ru.t1.kotchenko.tm.api.repository.ICommandRepository;
import ru.t1.kotchenko.tm.api.service.ICommandService;
import ru.t1.kotchenko.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommand() {
        return commandRepository.getTerminalCommand();
    }

}
